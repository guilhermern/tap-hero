﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Note : MonoBehaviour
{
    public Transform Target => target;
    public bool IsAlive => alive;
    
    [SerializeField] private Transform target;
    [SerializeField] private float currentDistanceToTarget;
    private bool alive = true;
    
    private float speed = 50f;
    private Vector3 currentPos = Vector3.zero;
    private Vector3 targetPos = Vector3.zero;

    private void Update()
    {
        if (!alive)
            return;

        Move();
        UpdateDistanceFromTarget();
    }

    private void Move()
    {
        transform.position += Vector3.down * (speed * Time.deltaTime);
    }

    private void UpdateDistanceFromTarget()
    {
        currentPos = transform.position;
        targetPos = target.transform.position;

        currentDistanceToTarget = Mathf.Sqrt(Mathf.Pow((targetPos.x - currentPos.x), 2f) + Mathf.Pow((targetPos.y - currentPos.y), 2f));
        
        //Debug.Log(currentDistanceToTarget);

        if (Math.Abs(currentDistanceToTarget) < 5f)
        {
            Debug.Log("I'm here");
            Debug.Break();
        }
    }



}
